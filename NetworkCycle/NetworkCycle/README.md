# NetworkCycle


## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NetworkCycle is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
  pod 'NetworkCycle'

```

##Usage 

1) Import the module in your development hierarchy,

```ruby
import NetworkCycle

```
2) Create a struct for ```AppNetworkConstants``` that contains base url, end points, status codes and error codes.

3) Create a class for ```AppNetworkManager``` to handle requests as per requirements.

4) Create separate operations by subclassing ```NetworkHeaderBase``` in order to initiate requests.

## Author

Roshan Baig (iOS Engineer, Onebyte LLC), roshanbaig17@gmail.com

## License

NetworkCycle is available under the MIT license. See the LICENSE file for more info.
