//
//  AppNetworkConstants.swift
//  swift-api-cycle
//
//  Copyright © 2017 Nauman Bhatti. All rights reserved.
//

import Foundation
import Alamofire
import Reachability

public class NetworkRequest {
    static public var baseURLString: String!
    static public var OAuthToken: String?
    static public var method: Alamofire.HTTPMethod?
    static public var path: String?
    
    static public var parameters: [String : Any]?
    
    static public var headers: [String : String]?
    
    
    class public func InitiateRequest() -> URLRequestConvertible {
        
        let url = URL(string: NetworkRequest.baseURLString)!
        var urlRequest = URLRequest(url: url.appendingPathComponent(NetworkRequest.path!))
        
        switch NetworkRequest.method! {
        case .get:
            urlRequest.httpMethod = "GET"
            
        case .put:
            urlRequest.httpMethod = "PUT"
        case .post:
            urlRequest.httpMethod = "POST"
        case .delete:
            urlRequest.httpMethod = "DELETE"
        default:
            urlRequest.httpMethod = "GET"
        }
        
        urlRequest.allHTTPHeaderFields = NetworkRequest.headers
        
        print("Header is \(String(describing: NetworkRequest.headers))")
        
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData

        if NetworkRequest.method! != .get{
            if let param = NetworkRequest.parameters{
            let data = try! JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                print(json)
            }
                urlRequest.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
            }
        }else{
            if let param = NetworkRequest.parameters{
            do{
                urlRequest = try URLEncoding.default.encode(urlRequest, with: param)
            }catch{
                print("url encoding error")
            }
            }
        }
        print("Request \(urlRequest)")
        return urlRequest as URLRequestConvertible
        
    }

    static func initiateQueryRequest(success: @escaping (AnyObject) -> (), errorHandler: @escaping (AnyObject) -> ()) {
        let urlString = "\(baseURLString!)\(path!)"
        let url = urlString.replacingOccurrences(of: " ", with: "%20")
        
        NetworkRequest.method = method
        var request = URLRequest(url: URL(string: url)!)
        
        switch NetworkRequest.method! {
        case .get:
            request.httpMethod = "GET"
        case .put:
            request.httpMethod = "PUT"
        case .post:
            request.httpMethod = "POST"
        case .delete:
            request.httpMethod = "DELETE"
        case .patch:
            request.httpMethod = "PATCH"
        default:
            request.httpMethod = "GET"
        }
        
        if parameters != nil {
            print("\(parameters!)")
            let  jsonData = try? JSONSerialization.data(withJSONObject: parameters as Any, options: .prettyPrinted)
            let theJSONText = String(data: jsonData!,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText ?? "")")
            
            request.httpBody = jsonData
        }
        
        
        request.allHTTPHeaderFields = self.headers
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            let response = response as? HTTPURLResponse
            
            guard let statusCode = response?.statusCode, statusCode == 200 else {
                if data != nil {
                    if response?.statusCode == 200{
                        success(data as AnyObject)
                        
                    }else{
                        //Error occured
                        success(data as AnyObject)
                    }
                    
                } else {
                    //Error occured
                    errorHandler(error as AnyObject)
                }
                
                return
            }
            success(data as AnyObject)
        }
        task.resume()
    }
    
    
}
