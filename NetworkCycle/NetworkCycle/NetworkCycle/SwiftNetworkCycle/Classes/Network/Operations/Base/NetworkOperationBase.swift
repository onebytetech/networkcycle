//
//  NetworkOperationBase.swift
//  swift-api-cycle
//
//  Copyright © 2017 Nauman Bhatti. All rights reserved.
//

import Foundation
import Reachability
import UIKit

open class NetworkOperationBase: NetworkOperationBaseSubclassing{
    //MARK: Instance Variables
    var networkTask : URLSessionDataTask?

    //MARK: States
    private var _executing : Bool = false
    override open var isExecuting : Bool {
        get { return _executing }
        set {
            guard _executing != newValue else { return }
            willChangeValue(forKey: "isExecuting")
            _executing = newValue
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    private var _finished : Bool = false
    override open var isFinished : Bool {
        get { return _finished }
        set {
            guard _finished != newValue else { return }
            willChangeValue(forKey: "isFinished")
            _finished = newValue
            didChangeValue(forKey: "isFinished")
        }
    }
    
    
    //MARK: Callbacks
    public var didFinishWithErrorCallback : ((_ error: AnyObject?) -> Void)?
    public var didFinishSuccessfullyCallback : ((_ responseObject: AnyObject?) -> Void)?
    
    //MARK: Overriden Methods
    override open func start() {
        do {
            if try Reachability().connection == .unavailable {
                self.networkTask?.cancel()
                DispatchQueue.main.async {
                    
                    let win = UIWindow(frame: UIScreen.main.bounds)
                    let vc = UIViewController()
                    vc.view.backgroundColor = .clear
                    win.rootViewController = vc
                    win.windowLevel = UIWindow.Level.alert + 1
                    win.makeKeyAndVisible()
                    let myAlert: UIAlertController = UIAlertController(title: "Alert", message: "Internet not available!", preferredStyle: .alert)
                    myAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        
                    }))
                    vc.present(myAlert, animated: true, completion: nil)
                }
            }
            else{
                DispatchQueue.main.async {
                    self.willChangeValue(forKey: "isExecuting")
                    self.isExecuting = true
                    self.didChangeValue(forKey: "isExecuting")
                }
            }
        } catch {
            print("NetworkCycleError: Reachability Error occured")
        }
        
    }
    
    override open func cancel() {
        super.cancel()
        self.networkTask?.cancel()
        
        //Obj-c
        self.safeCallDidFinishWithErrorCallback(error: nil);
    }
    
    //MARK: Public Methods
    public func finish() -> Void {
        DispatchQueue.main.async {
            self.willChangeValue(forKey: "isExecuting")
            self.willChangeValue(forKey: "isFinished")
            self.isExecuting = false
            self.isFinished = true
            self.didChangeValue(forKey: "isExecuting")
            self.didChangeValue(forKey: "isFinished")
        }
    }

    //MARK: Response Handling Methods
    open func safeCallDidFinishWithErrorCallback(error: AnyObject!) -> Void{
        DispatchQueue.main.async {
            if self.didFinishWithErrorCallback != nil {
                self.didFinishWithErrorCallback!(error)
            }
        }
    }
    
    open func safeCallDidFinishSuccessfullyCallback(responseObject: AnyObject) -> Void{
        
        DispatchQueue.main.async {
            if self.didFinishSuccessfullyCallback != nil {
                self.didFinishSuccessfullyCallback!(responseObject)
            }
        }
    }
}
