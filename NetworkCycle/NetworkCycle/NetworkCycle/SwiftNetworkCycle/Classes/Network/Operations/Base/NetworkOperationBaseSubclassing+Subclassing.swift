//
//  NetworkOperationBase+Subclassing.swift
//  swift-api-cycle
//
//  Copyright © 2017 Nauman Bhatti. All rights reserved.
//

import Foundation

open class NetworkOperationBaseSubclassing : Operation{
    //MARK: Public methods
    open func handleDidFinishedWithResponse(response: AnyObject!) -> Void{
        // To be overriden
    }
    
    open func handleDidFinishedUpdatingVoucherWithResponse(response: AnyObject!) -> Void{
        // To be overriden
    }
    
    open func handleDidFinishedWithError(error: AnyObject!) -> Void{
        // To be overriden
    }
    
    open func handleDidFinishedCommon() -> Void{
        // To be overriden
    }
}
