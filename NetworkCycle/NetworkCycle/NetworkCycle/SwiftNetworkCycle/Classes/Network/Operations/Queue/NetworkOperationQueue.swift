//
//  NetworkOperationQueue.swift
//  swift-api-cycle
//
//  Copyright © 2017 Nauman Bhatti. All rights reserved.
//

import Foundation
import Alamofire

open class NetworkOperationQueue : OperationQueue{
    
    //MARK: Class Variables
    static public let sharedInstance = NetworkOperationQueue()
    
    
}
