//
//  NetworkSessionManager.swift
//  swift-api-cycle
//
//  Copyright © 2017 Nauman Bhatti. All rights reserved.
//

import Foundation
import Alamofire

open class NetworkSessionManager : SessionManager{
    
    //MARK: Class Variables
    
    class public func request(_ urlRequest: URLRequestConvertible) -> DataRequest {
        return Alamofire.request(urlRequest)
    }
}
