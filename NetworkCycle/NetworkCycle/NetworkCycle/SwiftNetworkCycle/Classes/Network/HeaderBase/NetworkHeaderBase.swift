//
//  NetworkHeaderBase.swift
//  swift-api-cycle
//
//  Created by Roshan Baig on 1/14/19.
//  Copyright © 2019 Nauman Bhatti. All rights reserved.
//

import UIKit

open class NetworkHeaderBase: NetworkOperationBase {

    static public let sharedInstance: NetworkHeaderBase = {
        let instance = NetworkHeaderBase.init()
        return instance
    }()
    override public init() {
//        NetworkRequest.baseURLString = AppNetworkConstants.kBaseURLString
    }
    
    open func setCustomBaseUrl(baseUrl: String) {
        NetworkRequest.baseURLString = baseUrl
    }
    
    open func setCustomHeader(headers: [String: String]) {
        NetworkRequest.headers = headers
    }
    
    
}
