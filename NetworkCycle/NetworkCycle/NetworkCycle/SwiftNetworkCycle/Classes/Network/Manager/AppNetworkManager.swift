//
//  File.swift
//  swift-api-cycle
//
//  Copyright © 2018 Nauman Bhatti. All rights reserved.
//

import Foundation
import Alamofire

open class AppNetworkManager {
    // MARK: Class Variables
    static let sharedInstance = AppNetworkManager()
    public var baseUrlString = ""
    public var headers: [String : String]?
    public static func openNetworkRequest (methodType: Alamofire.HTTPMethod!, path: String?, parameters: [String : Any]?) -> URLRequestConvertible {
        NetworkRequest.path = path
        NetworkRequest.method = methodType
        NetworkRequest.parameters = parameters
        NetworkRequest.headers = ["Content-Type": "application/json"]
        
        
        return NetworkRequest.InitiateRequest()
    }
    
    public static func closeNetworkRequest (methodType: Alamofire.HTTPMethod!, path: String?, parameters: [String : Any]?) -> URLRequestConvertible {
        NetworkRequest.path = path
        NetworkRequest.method = methodType
        NetworkRequest.parameters = parameters
       
        
        return NetworkRequest.InitiateRequest()
    }
    
    public static func openNetworkQueryRequest (methodType: Alamofire.HTTPMethod!, path: String?, parameters: [String : Any]?, success: @escaping (AnyObject) -> (), errorHandler: @escaping (AnyObject) -> ()) {
        NetworkRequest.path = path
        NetworkRequest.method = methodType
        NetworkRequest.parameters = parameters
        NetworkRequest.headers = ["Content-Type": "application/json"]

        NetworkRequest.initiateQueryRequest(success: { responseObject in
            success(responseObject)
        }) { error in
            errorHandler(error)
        }
    }
    
    public static func closeNetworkQueryRequest (methodType: Alamofire.HTTPMethod!, path: String?, parameters: [String : Any]?, success: @escaping (AnyObject) -> (), errorHandler: @escaping (AnyObject) -> ()) {
        NetworkRequest.path = path
        NetworkRequest.method = methodType
        NetworkRequest.parameters = parameters
        NetworkRequest.initiateQueryRequest(success: { responseObject in
            success(responseObject)
        }) { error in
            errorHandler(error)
        }
    }   
}
